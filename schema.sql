create table messages (
    id bigint primary key not null,
    payload jsonb not null
);

create view messages_with_ts as
select id, payload, (to_timestamp(((id >> 22) + 1420070400000) / 1000.0)) as ts
from messages;

create index messages_ts on messages_with_ts ((to_timestamp(((id >> 22) + 1420070400000) / 1000.0)));
