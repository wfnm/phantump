module wooloo.farm/phantump

go 1.13

require (
	github.com/bwmarrin/discordgo v0.22.0
	github.com/gorilla/rpc v1.2.0 // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.8.0
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
)
