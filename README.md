# Phantump

Phantump is a Discord bot for tracking message updates/deletions. It will log all events configured channel containing the previous content of messages.

## Configuration

The following environment variables are used for configuration:

* `PHANTUMP_TOKEN`
  Discord token.

* `PHANTUMP_DBPATH`
  Path to store the database in.

* `PHANTUMP_TTL`
  Time-to-live for messages to be persisted. You may need to set this in accordance with both storage and legal considerations.

* `PHANTUMP_LOGCHANNELID`
  Channel for message updates/deletions to be logged to. **Messages in this channel will not be retained by Phantump.**

## Design

Due to a write heavy workload, Phantump uses an LSM-tree based database to store all messages: [Badger](https://github.com/dgraph-io/badger).

All messages are stored in big-endian byte sequences of their 64-bit Discord ID. This ensures roughly sequential inserts, as messages are received from Discord by snowflakes, which are roughly sequential (if two messages arrive at the same time, the insert might not be sequential, but good enough).

When Phantump connects to Discord, it will attempt to replay all messages in all known channels that are before the last message ID it stored, in order to have as much of the whole history as it can.

### Drawbacks

If a message is edited or deleted and Phantump is offline, when Phantump reconnnects, it will not know about the updated or deleted message. Phantump will not replay anything beyond the last message ID, and Discord provides no way of knowing if a message has since been edited or deleted.

Discord sometimes sends message update events without authors. Phantump will log this and fudge the author field using the author from the last known version of the message, if available.
