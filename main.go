package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"unicode/utf8"

	"github.com/bwmarrin/discordgo"
	"github.com/kelseyhightower/envconfig"
	"github.com/lib/pq"
	"golang.org/x/net/context"
)

type config struct {
	Token        string
	Dsn          string
	Status       string `default:""`
	LogChannelID string
}

type bot struct {
	c              *config
	session        *discordgo.Session
	preparedInsert *sql.Stmt
	db             *sql.DB
	replayID       uint64
}

const maxLength = 1024

func truncateString(s string, limit int) string {
	size := 0
	for index, r := range s {
		n := utf8.RuneLen(r) + index
		if n <= limit {
			size = n
		} else {
			break
		}
	}
	return s[:size]
}

func newBot(c *config) (*bot, error) {
	ctx := context.Background()

	session, err := discordgo.New(c.Token)
	if err != nil {
		return nil, err
	}

	session.StateEnabled = false
	session.SyncEvents = true

	session.Identify.Intents = discordgo.MakeIntent(
		discordgo.IntentsGuilds |
			discordgo.IntentsGuildMessages)

	db, err := sql.Open("postgres", c.Dsn)
	if err != nil {
		return nil, err
	}

	preparedInsert, err := db.PrepareContext(ctx, `
		insert into messages (id, payload)
		values ($1, $2)
	`)
	if err != nil {
		return nil, err
	}

	var replayID uint64
	if err := db.QueryRowContext(ctx, `
		select id
		from coalesce((
			select id
			from messages
			order by id desc
			limit 1
		), 0) id
	`).Scan(&replayID); err != nil {
		return nil, err
	}

	b := &bot{
		c:              c,
		session:        session,
		preparedInsert: preparedInsert,
		db:             db,
		replayID:       replayID,
	}
	b.session.AddHandler(b.ready)
	b.session.AddHandler(b.guildCreate)
	b.session.AddHandler(b.channelCreate)
	b.session.AddHandler(b.messageCreate)
	b.session.AddHandler(b.messageDelete)
	b.session.AddHandler(b.messageDeleteBulk)
	b.session.AddHandler(b.messageUpdate)
	return b, nil
}

func (b *bot) recordMessages(ctx context.Context, msgs []*discordgo.Message) (int, error) {
	n := 0

	// Reverse the msgs slice, as they arrive in OPPOSITE order.
	for i := len(msgs)/2 - 1; i >= 0; i-- {
		opp := len(msgs) - 1 - i
		msgs[i], msgs[opp] = msgs[opp], msgs[i]
	}

	for _, msg := range msgs {
		key, err := msgIDToKey(msg.ID)
		if err != nil {
			return n, err
		}

		value, err := json.Marshal(msg)
		if err != nil {
			return n, err
		}

		if _, err := b.preparedInsert.ExecContext(ctx, key, value); err != nil {
			return n, err
		}
		n++
	}
	return n, nil
}

func (b *bot) recordMessageDeletions(ctx context.Context, ids []string) error {
	keys := make([]uint64, len(ids))
	for i, id := range ids {
		key, err := msgIDToKey(id)
		if err != nil {
			return err
		}
		keys[i] = key
	}

	if _, err := b.db.ExecContext(ctx, `
		delete from messages
		where id = any($1)
	`, pq.Array(keys)); err != nil {
		return err
	}

	return nil
}

func (b *bot) shouldLog(channelID string) bool {
	return channelID != b.c.LogChannelID
}

func (b *bot) replayMessages(ctx context.Context, channelID string) error {
	if b.replayID == 0 {
		return nil
	}

	log.Printf("Replaying channel %s since %d.", channelID, b.replayID)
	var n int
	after := strconv.FormatUint(b.replayID, 10)
	for {
		msgs, err := b.session.ChannelMessages(channelID, 100, "", after, "")
		if err != nil {
			return err
		}

		if len(msgs) == 0 {
			break
		}

		after = msgs[0].ID

		m, err := b.recordMessages(ctx, msgs)
		if err != nil {
			return err
		}
		n += m
	}
	log.Printf("Replaying %d messages in channel %s.", n, channelID)

	return nil
}

func (b *bot) ready(_ *discordgo.Session, event *discordgo.Ready) {
	go b.session.UpdateStatus(0, b.c.Status)
}

func (b *bot) guildCreate(_ *discordgo.Session, event *discordgo.GuildCreate) {
	ctx := context.Background()

	log.Printf("Replaying guild %s.", event.Guild.ID)
	var wg sync.WaitGroup
	for _, channel := range event.Channels {
		if !b.shouldLog(channel.ID) {
			continue
		}

		channelID := channel.ID

		wg.Add(1)
		go func() {
			channelID := channelID
			if err := b.replayMessages(ctx, channelID); err != nil {
				log.Printf("Failed to replay messages from %s: %s", channelID, err)
			}
			wg.Done()
		}()
	}
	wg.Wait()

	log.Printf("Finished replaying guild %s.", event.Guild.ID)
}

func (b *bot) channelCreate(_ *discordgo.Session, event *discordgo.ChannelCreate) {
	ctx := context.Background()

	if !b.shouldLog(event.ID) {
		return
	}

	if err := b.replayMessages(ctx, event.ID); err != nil {
		log.Printf("Failed to replay messages from %s: %s", event.ID, err)
	}
}

func msgIDToKey(msgID string) (uint64, error) {
	return strconv.ParseUint(msgID, 10, 64)
}

func (b *bot) message(ctx context.Context, messageID string) (*discordgo.Message, error) {
	key, err := msgIDToKey(messageID)
	if err != nil {
		return nil, err
	}

	var raw []byte
	if err := b.db.QueryRowContext(ctx, `
		select payload
		from messages
		where id = $1
	`, key).Scan(&raw); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	msg := &discordgo.Message{}
	if err := json.Unmarshal(raw, msg); err != nil {
		return nil, err
	}

	return msg, nil
}

func (b *bot) messageCreate(_ *discordgo.Session, event *discordgo.MessageCreate) {
	ctx := context.Background()

	if !b.shouldLog(event.ChannelID) {
		return
	}

	if _, err := b.recordMessages(ctx, []*discordgo.Message{event.Message}); err != nil {
		log.Printf("Failed to record message create %s: %s", event.ID, err)
	}
}

func (b *bot) messageUpdate(_ *discordgo.Session, event *discordgo.MessageUpdate) {
	ctx := context.Background()

	if !b.shouldLog(event.ChannelID) {
		return
	}

	lastMsg, err := b.message(ctx, event.ID)
	if err != nil || lastMsg == nil {
		log.Printf("Failed to retrieve message %s: %s", event.ID, err)
	} else {
		if event.Author == nil {
			event.Message.Author = lastMsg.Author
		}

		if !event.Author.Bot && lastMsg.EditedTimestamp != event.EditedTimestamp {
			go func() {
				fields := []*discordgo.MessageEmbedField{}
				if lastMsg.Content != "" {
					fields = append(fields, &discordgo.MessageEmbedField{
						Name:  "Before",
						Value: truncateString(lastMsg.Content, maxLength),
					})
				}
				if event.Content != "" {
					fields = append(fields, &discordgo.MessageEmbedField{
						Name:  "After",
						Value: truncateString(event.Content, maxLength),
					})
				}

				if _, err := b.session.ChannelMessageSendComplex(b.c.LogChannelID, &discordgo.MessageSend{
					Embed: &discordgo.MessageEmbed{
						Color:       0xffff00,
						Description: fmt.Sprintf("**Message by <@%s> edited in <#%s>**\n[Jump](https://discordapp.com/channels/%s/%s/%s)", lastMsg.Author.ID, lastMsg.ChannelID, event.GuildID, event.ChannelID, event.ID),
						Fields:      fields,
						Footer:      &discordgo.MessageEmbedFooter{Text: fmt.Sprintf("%s • Original message was posted at", event.Message.ID)},
						Timestamp:   string(lastMsg.Timestamp),
					},
				}); err != nil {
					log.Printf("Failed to send message update log event for %s (before = %s, after = %s): %s", event.ID, lastMsg.Content, event.Content, err)
				}
			}()
		}
	}

	if _, err := b.recordMessages(ctx, []*discordgo.Message{event.Message}); err != nil {
		log.Printf("Failed to record message update %s: %s", event.ID, err)
	}
}

func (b *bot) messageDelete(_ *discordgo.Session, event *discordgo.MessageDelete) {
	ctx := context.Background()

	if !b.shouldLog(event.ChannelID) {
		return
	}

	lastMsg, err := b.message(ctx, event.ID)
	if err != nil || lastMsg == nil {
		log.Printf("Failed to retrieve message %s: %s", event.ID, err)
	} else {
		if !lastMsg.Author.Bot {
			fields := []*discordgo.MessageEmbedField{}
			if lastMsg.Content != "" {
				fields = append(fields, &discordgo.MessageEmbedField{
					Name:  "Before",
					Value: truncateString(lastMsg.Content, maxLength),
				})
			}

			go func() {
				if _, err := b.session.ChannelMessageSendComplex(b.c.LogChannelID, &discordgo.MessageSend{
					Embed: &discordgo.MessageEmbed{
						Color:       0xff0000,
						Description: fmt.Sprintf("**Message by <@%s> deleted in <#%s>**\n[Jump](https://discordapp.com/channels/%s/%s/%s)", lastMsg.Author.ID, lastMsg.ChannelID, event.GuildID, event.ChannelID, event.ID),
						Fields:      fields,
						Footer:      &discordgo.MessageEmbedFooter{Text: fmt.Sprintf("%s • Original message was posted at", event.ID)},
						Timestamp:   string(lastMsg.Timestamp),
					},
				}); err != nil {
					log.Printf("Failed to send message delete log event for %s (before: %s): %s", event.ID, lastMsg.Content, err)
				}
			}()
		}
	}

	if err := b.recordMessageDeletions(ctx, []string{event.ID}); err != nil {
		log.Printf("Failed to record message delete %s: %s", event.ID, err)
	}
}

func (b *bot) messageDeleteBulk(_ *discordgo.Session, event *discordgo.MessageDeleteBulk) {
	ctx := context.Background()

	if !b.shouldLog(event.ChannelID) {
		return
	}

	for _, id := range event.Messages {
		id := id

		lastMsg, err := b.message(ctx, id)
		if err != nil || lastMsg == nil {
			log.Printf("Failed to retrieve message %s: %s", id, err)
		} else {
			if !lastMsg.Author.Bot {
				fields := []*discordgo.MessageEmbedField{}
				if lastMsg.Content != "" {
					fields = append(fields, &discordgo.MessageEmbedField{
						Name:  "Before",
						Value: truncateString(lastMsg.Content, maxLength),
					})
				}

				go func() {
					if _, err := b.session.ChannelMessageSendComplex(b.c.LogChannelID, &discordgo.MessageSend{
						Embed: &discordgo.MessageEmbed{
							Color:       0xff0000,
							Description: fmt.Sprintf("**Message by <@%s> deleted in <#%s> (bulk delete)**\n[Jump](https://discordapp.com/channels/%s/%s/%s)", lastMsg.Author.ID, lastMsg.ChannelID, event.GuildID, event.ChannelID, id),
							Fields:      fields,
							Footer:      &discordgo.MessageEmbedFooter{Text: fmt.Sprintf("%s • Original message was posted at", id)},
							Timestamp:   string(lastMsg.Timestamp),
						},
					}); err != nil {
						log.Printf("Failed to send message delete log event for %s (before: %s): %s", id, lastMsg.Content, err)
					}
				}()
			}
		}
	}

	if err := b.recordMessageDeletions(ctx, event.Messages); err != nil {
		log.Printf("Failed to record message bulk delete %v: %s", event.Messages, err)
	}
}

func (b *bot) run() error {
	log.Printf("Connecting to Discord.")
	if err := b.session.Open(); err != nil {
		return err
	}

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	b.session.Close()
	b.db.Close()
	return nil
}

func main() {
	var c config
	if err := envconfig.Process("phantump", &c); err != nil {
		log.Fatalf("Failed to parse config: %s", err)
	}

	bot, err := newBot(&c)
	if err != nil {
		log.Fatalf("failed to create discord client: %s", err)
	}

	if err := bot.run(); err != nil {
		log.Fatalf("failed to connect to discord: %s", err)
	}
}
